pub mod greetings
{
    pub mod english; // ./Phrases/src/greetings/english.rs
    pub mod french
    {
      pub fn hello() -> String { "bonjour".to_string() }
      pub fn goodbye() -> String { "au revoir".to_string() }
    }
}
