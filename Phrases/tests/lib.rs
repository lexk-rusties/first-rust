#[cfg(test)]
mod tests{

extern crate phrases;


#[test]
fn english_greeting_correct(){
  assert_eq!("hello", phrases::greetings::english::hello());
}
#[test]
#[should_panic]
fn englishs_greeting_correct(){
  assert_eq!("hellos", phrases::greetings::english::hello());
}
#[test]
#[should_panic]
//#[ignore]
fn eng_greeting_correct(){
  assert_eq!("hello", phrases::greetings::english::goodbye());
}
}
