use std::mem;

fn double_value(v: i32) -> i32
{
    v*2
}

pub fn main_dbg(){
    let mut x = 3;
    x = double_value(x);
    x = 42;
}
