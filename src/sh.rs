#![allow(dead_code)]
use std::mem;
struct Point{
    x: f64,
    y: f64
}

fn origin() -> Point{
    Point{x: 0.0, y: 0.0}
}

pub fn stack_and_heap(){
    println!("Hello, sh!");

    let p1 = origin(); // :Point - the STACK allocated
    let p2 = Box::new(origin()); // :Box<Point> - allocated to the HEAP

    println!("p1 takes up {} bytes on stack", mem::size_of_val(&p1));
    println!("p2 takes up {} bytes on stack", mem::size_of_val(&p2));
    // p2 has fewer bytes bcs its only a pointer
    // p2 takes this to the HEAP where actual value is stored

    let p3 = *p2; // * follows the pointer where the boxed value is
    // unboxing and relocating it back to the STACK
    println!("x coordinate of p3 is {}", p3.x);

}
