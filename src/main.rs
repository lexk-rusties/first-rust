use std::fmt::{Debug, format};
use std::{mem, thread, time};
use std::env::join_paths;
//use std::iter::SourceIter;
use std::ops::{AddAssign, Neg};
use std::sync::{Arc, Mutex};
use phrases::greetings::french;
use rand::{Rng, thread_rng};

mod sh;
mod dbg_clion;
mod pattern_matching;
mod number_guessing_game;


const MEANING_OF_LIFE:u8 = 42; // no fixed address
static Z:i32 = 123;
static mut KZ:i32 = 314; // mutable - works only with 'unsafe' mode

fn types_and_variables(){

    fn data_types() {
        println!("Hello, world!");

        let a: u8 = 123; // u = unsigned, 7 bits, 0 - 255
        println! ("a = {}", a); // immutable

        // u = unsigned, 0 to 2^N-1
        // i = signed, -2^(N-1) .. 2&(N-1)-1
        let mut b: i8 = 0; // -128 -- 127
        println!("b = {} before", b);
        b = 42;
        println!("b = {} after", b);

        let mut c = 123456789; // i32 = 32 bits = 5 bytes
        println!("c = {}, takes up {} bytes", c, mem::size_of_val(&c));
        c = -1;
        println!("c = {}, takes up {} bytes", c, mem::size_of_val(&c));

        // u8, u16, u32, u64, i8, i16, ...

        // usize isize
        let z: isize = 123;
        let size_of_z = mem::size_of_val(&z);
        println!("z = {}, takes up {} bytes, {}-bit OS",
                 z, size_of_z, size_of_z*8);

        let d: char = 'x'; // could be even . ;
        println!("{} is a char, size = {} bytes", d, mem::size_of_val(&d));

        // f32 f64 iEEE754 - (std for numbers), all signed!

        let e: f32 = 2.5;
        println!("{}, size = {} bytes", e, mem::size_of_val(&e));
        let de = 3.5; // f64
        println!("{}, size = {} bytes", de, mem::size_of_val(&de));

        let g: bool = false; // true
        println!("{}, size = {} bytes", g, mem::size_of_val(&g));
    }
    fn operators(){

        // arithmetic
        let mut a = 2+3*4; // +-*/
        println!("a = {}", &a);
        a = a+1; // can't use ++ --
        a -= 2; // a = a -2;
        // -= += *= /= %=
        println!("remainder of {} / {} = {}", a, 3, (a%3));

        let a_cubed = i32::pow(a, 3); // calc cube of a
        println!("{} cubed is = {}", a, a_cubed);

        let b = 2.5; // f64
        let b_cubed = f64::powi(b, 3); // normal power calculations
        let b_to_pi = f64::powf(b, std::f64::consts::PI); // logarithmic calculations
        println!("{} cubed = {}, {}^PI = {}", b, b_cubed, b, b_to_pi);

        // bitwise
        let c = 1 | 2; // | OR & AND ^ XOR ! NOR
        // 01 OR 10 = 11 == 3_10
        println!("1 | 2 = {}", c);
        let two_to_pow_10 = 1 << 10; // >>
        println!("2^10 = {}", two_to_pow_10); // power calculations where places shifted on a bits lvl

        // logical operators
        let pi_less_4 = std::f64::consts::PI < 4.0; // true
        println!("pi < 4 that's {}", pi_less_4);
        // > <= >=
        let x = 5;
        let x_is_5 = x == 5; //true
        println!("{} = 5 that's {}", x, x_is_5);
    }
    fn scope_shadowing(){
        let a = 123;

        println!("a is outside {}", a);

        {
            let b = 456; // exist only inside of curly braces {} and can't be referenced outside
            println!("inside b = {}", b);

            println!("inside old a = {}", a);
            let a = 555; // changing the value here changes it only for scope of braces
            println!("inside new a = {}", a);
        }
        println!("a is outside = {}", a); // we can see the old value untouched

        let a = 9999; // redefining is only upd new value for the same variable and doesn't crush it
        println!("a is outside & redefined = {}", a); // we can see the old value untouched

    }
    fn constants(){

        println!("constant meaning of life = {}", MEANING_OF_LIFE); //declared in the beginning with key 'const'
        println!("static constant Z = {}", Z); //declared in the beginning with key 'static'

        unsafe // without it can't use of mutable static
            // promise to be careful to compiler, think that better to be avoided
            {
                println!("unsafe mutable constant KZ  = {}", KZ);
                KZ = 89;
                println!("unsafe mutable constant KZ was reassigned to  = {}", KZ) // will prob print it ;)
            }

    }

    //data_types();
    //operators();
    //scope_shadowing();
    //constants();
    //sh::stack_and_heap();
    //dbg_clion::main_dbg();
}

fn control_flow(){

    fn if_statement(){
        let temp = 35;
        if temp > 30{
            println!("really hot outside");
        }
        else if temp < 10{
            println!("really cold!");
        }
        else{
            println!("temperature is OK");
        }
        let day = if temp > 20 {"sunny"} else {"cloudy"}; // &str string declaration auto catch up
        println!("today is {}", day);

        println!("is it {} outside",
            if temp > 20 {"hot"} else if temp <10 {"cold"} else {"OK"});
        println!("it is {}",
            if temp > 20 {
                if temp > 30 {"very hot"} else {"hot"}
            } else if temp < 10 {"cold"} else {"OK"});
    }
    fn while_and_loop(){
        let mut x =1;

         while x < 1000{
             x *= 2;

             if x == 64 { continue; } // skips ln after statement and take next iter. of while

             println!("x = {}", x);
         }

        let mut y = 1;
        loop // while true
        {
            y *= 2;
            println!("y = {}", y);

            if y == 1<<10 { break; } // jumps out of infinite loop
            // 1<<10 2 to pow of 10
        }
    }
    fn for_loop(){
        for x in 1..11{
            if x == 3 { continue; }
            if x == 8 { break; }
            println!("x = {}", x);
        }

        for (pos,y) in (30..41).enumerate(){ // counter of position and var itself
            println!("{}: {}", pos,y);
        }
    }
    fn match_statement(){
        // pretty much is IF but can test several different cases OR a range of cases
        // you have to cover all possibilities, if you don't - it wouldn't compile!!!
        let country_code = 1001;

        let country = match country_code {
            44 => "UK",
            46 => "Sweden",
            7 => "Russia",
            1..=1000 => "unknown",
            _ => "invalid"
        };
        println!("the country with code {} is {}", country_code, country);

        let x = false; // :bool

        let s = match x { // :&str
            true => "yes",
            false => "no"
        };
        println!("result from bool match of x = {} is {}", x, s);
    }
    fn combination_lock(){
        use std::io::stdin;

        enum State{
            Locked,
            Failed,
            Unlocked
        }

        let code = String::from("1234");
        let mut state = State::Locked;
        let mut entry = String::new();

        loop{
            match state{
                State::Locked => {
                    let mut input = String::new();
                    match stdin().read_line(&mut input){
                        Ok(_) => {
                            entry.push_str(&input.trim_end());
                        }
                        Err(_) => continue
                    }

                    if entry == code {
                        state = State::Unlocked;
                        continue;
                    }
                    if !code.starts_with(&entry){
                        state = State::Failed;
                    }
                }
                State::Failed => {
                    println!("FAILED");
                    entry.clear(); // "" empty str
                    state = State::Locked;
                    continue;
                }
                State::Unlocked => {
                    println!("UNLOCKED");
                    return;
                }
            }
        }

    }

    //if_statement();
    //while_and_loop();
    //for_loop();
    //match_statement();
    //combination_lock();
}

struct Point
{
    x: f64,
    y: f64
}
struct Line
{
    start: Point,
    end: Point
}

fn data_structures(){

    fn structures(){
        let p = Point{x: 3.0, y: 4.0};
        println!("point p is at ({}, {})", p.x, p.y);

        let p2 = Point {x: 5.0, y: 10.0};
        println!("point p2 is at ({}, {})", p2.x, p2.y);

        let myline = Line { start: p, end: p2};
        println!("My line is starts at ({}, {}) and ends at ({}, {})",
                 myline.start.x, myline.start.y,
                 myline.end.x, myline.end.y);
    }
    enum Color
    {
        Red,
        Green,
        Blue,
        RgbColor(u8, u8, u8), //tuple
        CMYKColor{cyan:u8, magenta: u8, yellow: u8, black: u8} // struct
    }
    fn enumeration(){
        let c:Color = Color::CMYKColor {cyan: 0, magenta: 128, yellow: 0, black: 0};
        //let c:Color = Color::RgbColor(0,0,0);
        //let c:Color = Color::Red;
        match c
        {
            Color::Red => println!("r"),
            Color::Green => println!("g"),
            Color::Blue => println!("b"),
            Color::RgbColor(0, 0, 0)
            | Color::CMYKColor{cyan: _, magenta: _, yellow: _, black: 255} => println!("black"),
            Color::RgbColor(r,g,b) => println!("rgb({}, {}, {})", r,g,b),
            _ => println!("Not Listed Color")
        }
    }
    union IntOrFloat //32 bits
    {
        i: i32,
        f: f32
    }
    fn unions(){

        fn process_value(iof: IntOrFloat){
            unsafe {
                match iof {
                    IntOrFloat { i: 42 } => {
                        println!("meaning of life value");
                    }
                    IntOrFloat { f } => {
                        println!("value = {}", f);
                        // floating point while println! could be printed as int if its ex. 42.0
                    }
                }
            }
        }


        let mut iof = IntOrFloat{ i: 123 };
        iof.i = 234;

        let value = unsafe { iof.i }; // could be an float putted there - crash logic
        println!("iof.i = {}", value);
        process_value(IntOrFloat{i:5});
    }
    fn option_t_if_let_while_let(){
        let x =3.0;
        let y = 2.0;

        // Option -> Some(v) | None
        let result =
            if y != 0.0 { Some(x/y)} else { None };
        match result {
            Some(z) => println!("{}/{}={}", x,y,z),
            None => println!("cannot divide by zero")
        }
        if let Some(z) = result {
            println!("result = {}", z);
        }
    }
    fn arrays(){

        let mut a: [i32;5] = [1,2,3,4,5];

        println!("a has {} elements, the first is {}",
                 a.len(), a[0]);
        a[0] = 321;
        println!("a[0] = {}", a[0]);

        println!("{:?}", a); // {:?} debug app used to iterate and print it

        if a == [1,2,3,4,5]{
            println!("arrays doesn't match");
        } else { println!("arrays are match");
        }

        let b = [1u16; 10]; // b.len() == 10, specified u16
        for i in 0..b.len(){
            println!("{}", b[i]);
        }
        println!("b took up {} bytes", mem::size_of_val(&b));

        let mtx:[[f32;3];2] = // two dimensional array | matrix
        [
            [1.0, 0.0, 0.0],
            [0.0, 2.0, 0.0]
        ];
        println!("mtx array:\n{:?}", mtx);

        for i in 0..mtx.len() // diagonal elements
        {
            for j in 0..mtx[i].len()
            {
                if i == j
                {
                    println!("mtx[{}][{}] = {}", i, j, mtx[i][j]);
                }
            }
        }
    }
    fn slices(){
        let mut data: [i32;5] = [1,2,3,4,5]; // orig array
        fn use_slice(slice: &mut[i32]){ // mutable slice with var n of elements
            println!("first element = {}, len = {}", slice[0], slice.len());
            slice[0] = 4321;
        }
        use_slice(&mut data[1..4]);
        //use_slice(&mut data); // entire array as a slice
        println!("{:?}", data);
        }
    fn tuples(){

        fn sum_and_product(x: i32, y: i32) -> (i32, i32){

            (x+y, x*y)
        }

        let x = 3;
        let y = 4;
        let sp = sum_and_product(x, y);

        println!("sp  = {:?}", sp);
        println!("{0} + {1} = {2},\n{0} * {1} = {3}", x, y, sp.0, sp.1);

        // destructuring
        let (a,b) = sp;
        println!("a = {}, b = {}", a ,b);

        let sp2 = sum_and_product(4, 7);

        let combined = (sp, sp2);
        println!("{:?}", combined);

        println!("th last element = {}", combined.1.1); // recommended using (combined.1).1

        let ((c,d), (e,f)) = combined;
        let foo = (true, 42.0, -1i8); // can combine diff types in one tuple
        println!("{:?}",foo);

        let meaning = (42,);
        println!("{:?}", meaning);

    }
    struct PointT<T, V>
    {
        x: T,
        y: V
    }
    struct LineT<T>
    {
        start: PointT<T,T>,
        end: PointT<T,T>
    }
    fn generics(){
        let a = PointT{ x: 0f64, y: 4.0 };
        //let a:PointT<u16, i32> = PointT{ x: 0, y: 4 };
        let b: PointT<f64, f64> = PointT { x: 1.2, y: 3.4 };
        //let b: PointT<f64, f32> = PointT { x: 1.2, y: 3.4 };
        let myline = LineT { start: a, end: b };
    }

    //structures();
    //enumeration();
    //unions();
    //option_t_if_let_while_let();
    //arrays();
    //slices();
    //tuples();
    //pattern_matching::pattern_matching();
    //generics();
}

fn standard_collections(){

    fn vectors(){

        let mut a= Vec::new();

        a.push(1);
        a.push(2);
        a.push(3);

        println!("a = {:?}", a);

        a.push(44);

        //usize isize

        let idx:usize = 1;

        //below crashes program bcs of no element index 111
        //let idx:usize = 111;

        //below can't be used as machine is 64b & signed i32 can have neg values
        //let idx:i32 = 0;

        a[idx] = 312;

        println!("a = {:?}", a);
        println!("a[0] = {}", a[idx]);

        //Option type
        match a.get(6)
        {
            Some(x) => print!("a[6] = {}", x),
            None => println!("error: No such element")
        }

        for x in &a { println!("{}", x); }

        a.push(77);
        println!("{:?}",a);

        //Option type - Some(77)
        let last_elem = a.pop();
        println!("last elem is {:?}, a = {:?}", last_elem, a);

        //will cause error[E0005]: refutable pattern in local binding: `None` not covered
        //let Some(last_value) = a.pop();

        while let Some(x) = a.pop()
        {
            println!("{}", x);
        }

    }

    fn hash_map(){

        use std::collections::HashMap;

        let mut shapes = HashMap::new();
        shapes.insert(String::from("triangle"), 3);
        shapes.insert(String::from("square"), 4);

        println!("a square has {} sides", shapes["square".into()]);

        for (key, value) in &shapes {
            println!("{} : {}", key, value);
        }
        shapes.insert("square".into(), 5);
        println!("after mod square: {:?}", shapes);

        shapes.entry("circle".into()).or_insert(1);

        println!("after mod circle: {:?}", shapes);

        {
            let actual = shapes.entry("circle".into()).or_insert(2);
            *actual = 0;
        }

        println!("after checking/mod circle: {:?}", shapes);

    }

    fn hash_set(){
        use std::thread;
        use std::time;
        use std::collections::HashSet;

        let mut greeks = HashSet::new();
        greeks.insert("gamma");
        greeks.insert("delta");

        println!("{:?}", greeks); // order is reversed - LIFO!

        greeks.insert("delta"); // doesn't make a change, bcs HS stores only the unique elem
        println!("{:?}", greeks);
        let letters_input = ["vega", "delta"];

        for i in letters_input{
            let added_vega = greeks.insert(i);
            if added_vega {
                println!("we added {}! hooray: {:?}", i, greeks);
            } else {
                println!("skipping {}, it's already exist!: {:?}", i, greeks)
            }
        }

        if !greeks.contains("kappa"){
            println!("we don't have kappa");
        }

        let removed = greeks.remove("delta");
        if removed { println!("we removed delta!: {:?}", greeks) };

        let _1_5: HashSet<_> = (1..=5).collect();
        let _6_10: HashSet<_> = (6..=10).collect();
        let _1_10: HashSet<_> = (1..=10).collect();
        let _2_8: HashSet<_> = (2..=8).collect();

        // subset
        println!(
            "is {:?} a subset of {:?}? {}",
            _2_8, _1_10, // order is completely random
            _2_8.is_subset(&_1_10)
        );

        // disjoint = no common elements
        println!(
            "is {:?} a disjoint of {:?}? {}",
            _1_5, _6_10, // order is completely random
            _1_5.is_disjoint(&_6_10)
        );

        // union, intersection == present in both
        println!(
            "items in eathier {:?} and {:?} are {:?}",
            _2_8, _6_10, // order is completely random
            _2_8.union(&_6_10)
        );
        println!(
            "items in both {:?} and {:?} are {:?}",
            _2_8, _6_10, // order is completely random
            _2_8.intersection(&_6_10)
        );

        // difference
        // symmetric_difference = union - intersections
        println!(
            "not common items for {:?} and {:?} are {:?}",
            _2_8, _6_10, // order is completely random
            _2_8.difference(&_6_10)
        );
        println!(
            "items in that are not in intersection for {:?} and {:?} are {:?}",
            _2_8, _6_10, // order is completely random
            _2_8.symmetric_difference(&_6_10)
        );






    }

    fn iterate(){

        let mut vec = vec![3, 2, 1];

        // iteration with numbers
        for x in &vec
        {
            println!("{}", *x);
        }

        // iteration via .iter
        for k in vec.iter() {
            println!("we got {}", *k);
            // referencing by asterisk in this case is actually optional
            // RUST will not change the ownership bcs of .iter usage - MAGIC ;)
            //k += 1 // can't modify it
            }

        // mutable loop with iter
        for z in vec.iter_mut() {
            *z +=2;
        }
        println!("increased vector by 2: {:?}", vec);

        // reverse order iter
        for o in vec.iter().rev() {
            println!("we got {}", *o);
        }

        // move iteration

        let mut vec2 = vec![1,2,3];
        //let it = vec.into_iter(); // borrow error[E0382]: use of moved value: `vec`
        vec2.extend(vec);
        println!("{:?}", vec2);


    }

    //vectors();
    //hash_map();
    //hash_set();
    //iterate();


}

fn characters_strings(){

    fn strings(){

        // utf-8
        let s:&'static str = "hello there!"; // &str = string slice
        // s = "abc"; // will not work
        //let h = s[0]; // string indices are ranges of `usize`
        //println!("{}", h); // error[E0277]: the type `str` cannot be indexed by `{integer}`

        for c in s.chars().rev()
        {
            println!("{}", c)
        }
        if let Some(first_char) = s.chars().nth(0) // if we really want to have 1st char
        {
            println!("first letter is {}", first_char)
        }

        // heap allocated
        // String
        let mut letters = String::new();
        let mut a = 'a' as u8;
        while a <= ('z' as u8)
        {
            letters.push(a as char);
            letters.push_str(",");
            a += 1;
        }
        println!("lets print whole alphabet:\n{}", letters);

        // &str <> String
        let u:&str = &letters;
        println!("conversion from '&str' to 'String':\n {}", u);

        // concatenation
        // String + str = will work

        let zu = String::from(u);
        let z = zu + "abc";
        println!("concatenated alphabet with abc with simple '+' operator\n{}", z);

        let mut abc = String::from("hello world");
        let mut cba = "hello world".to_string();

        let m = z + &cba;
        println!("converted by String::from: {},
     converted by .to_string: {}
     concatenation alph+abc+hw:\n{}",
                 abc, cba,m);

        cba.remove(0);
        cba.push_str("!!!");

        println!("after .remove by idx 0, add by .push '!!!' and replacing of 'ello': {}",
                 cba.replace("ello", "Good bye"));
    }

    fn string_formatting(){
        let name = "Alex";
        let greeting = format!("hi, I'm {}, nice to meet you", name);
        println!("{}", greeting);

        let hello = "hello";
        let rust = "rust";
        let hello_rust = format!("{}, {}!", hello, rust);
        println!("{}", hello_rust);

        let run = "run";
        let forest = "forest";
        let rfr = format!("{0}, {1}, {0}!", run, forest);
        println!("{}", rfr);

        let info = format!(
            "the name's {last}. {first} {last}.",
            first = "james",
            last  = "bond"
        );
        println!("{}", info);

        let mixed = format!(
            "{1} {} {0} {} {data}", // beta alpha alpha beta delta
            "alpha",
            "beta",
            //"gamma", // error: argument never used
            data = "delta"
        );
        println!("{}", mixed);


    }

    //strings();
    //string_formatting();
    //number_guessing_game::number_guess();
}

fn functions_and_arguments(){

    fn functions(){

        fn print_value(x: i32){
            println!("value = {}", x)
        }

        fn increase(x: &mut i32){
            *x +=1;
        }

        fn product(x: i32, y: i32) -> i32 { // -> i32 specifies the return type
            x * y // return line WITHOUT the semicolon ;
        }

        print_value(33);

        let mut z = 1;
        increase(&mut z);
        println!("z = {}", z);

        let a =3;
        let b = 5;
        let p = product(a, b);

        println!("{} * {} = {}", a, b, p);
    }
    fn functions_arg(){

        let p = Point{x: 3.0, y: 4.0};
        println!("point p is at ({}, {})", p.x, p.y);

        let p2 = Point {x: 5.0, y: 10.0};
        println!("point p2 is at ({}, {})", p2.x, p2.y);

        let myline = Line { start: p, end: p2};
        println!("My line is starts at ({}, {}) and ends at ({}, {})",
                 myline.start.x, myline.start.y,
                 myline.end.x, myline.end.y);

        impl Line
        {
            fn len(&self) -> f64 {
                let dx = self.start.x - self.end.x;
                let dy = self.start.y - self.end.y;
                (dx*dx+dy*dy).sqrt()
            }
        }

        println!(" length = {}", myline.len());

    }
    fn closures(){
        fn say_hello(){ println!("hello!")}

        let sh = say_hello; // without rounding brackets, to pass fun not a result
        sh(); // call var as usual fun

        let plus_one = |x:i32| -> i32 { x + 1 }; // inline declaration of fun
        let a = 6;
        println!("{} + 1 = {}", a, plus_one(a));

        let mut two = 2;
        { // encapsulated with brackets to have scope been destroyed before use of var two below
            let plus_two = |x| {
                let mut z = x;
                z += two;
                z
            };
            println!("{} + 2 = {}", 3, plus_two(3));
        }
        let borrow_two = &mut two;

        // T: by value
        // T&
        // &mut &

        let plus_three = |x: &mut i32| *x +=3;
        let mut f = 12;
        plus_three(&mut f);
        println!("f = {}", f);
    }
    fn high_order_functions(){
        // functions that take functions
        // f(g) { let x = g(); }

        // functions that return functions
        // generators
        //f() -> g()

        //sum of all even squares < 500

        let limit = 500;
        let mut sum = 0;

        fn greater_that(limit: u32)
            -> impl Fn(u32) -> bool {
            move |y| y > limit
        }

        let above_limit = greater_that(limit);

        fn is_even(x: u32) -> bool {
            x % 2 == 0
        }

        for i in 0.. {
            let isq = i*i;

            if above_limit(isq){
                break;
            } else if is_even(isq){
                sum += isq;
            }
        }

        println!("loop sum = {}", sum);

        let sum2 = (0..)
            .map(|x| x*x)
            .take_while(|&x| x < limit)
            .filter(|x:&u32| is_even(*x))
            .fold(0, |sum, x| sum + x);
        println!("higher sum = {}", sum2);



    }

    //functions();
    //functions_arg();
    //closures();
    //high_order_functions();


}

fn traits(){
    trait Animal // used in traits_intr() + vec_of_diff_obj()
    {
        fn create(name: &'static str) -> Self where Self: Sized;
        fn name(&self) -> &'static str;
        fn talk(&self){
            println!("{} cannot talk", self.name());
        }
    }
    struct Human // same for traits_intr() + vec_of_diff_obj()
    {
        name: &'static str
    }
    struct Cat // same for traits_intr() + vec_of_diff_obj()
    {
        name: &'static str
    }

    fn traits_intr(){

        impl Animal for Human
        {
            fn create(name: &'static str) -> Human {
                Human { name } // {name: name} Expression can be simplified
            }
            fn name(&self) -> &'static str {
                self.name
            }
            fn talk(&self){
                println!("{} says hello", self.name)
            }
        }

        impl Animal for Cat
        {
            fn create(name: &'static str) -> Cat {
                Cat { name } //  {name: name} Expression can be simplified
            }
            fn name(&self) -> &'static str {
                self.name
            }
            fn talk(&self){
                println!("{} says meow", self.name)
            }
        }
        trait Summable<T> // declaration used to impl further
        {
            fn sum(&self) -> T;
        }
        impl Summable<i32> for Vec<i32> // impl for ANY vectors of i32
        {
            fn sum(&self) -> i32 {
                let mut result:i32 = 0;
                for x in self { result += *x; }
                return result
            }
        }
        //let h = Human{name: "John"}; // same below but with create fun
        let h: Human = Animal::create("John");
        // with specialization of : Human type upward, we can make it work from Animal
        h.talk();
        let c = Cat{name: "Misty"};
        c.talk();

        let a = vec![1,2,3];
        println!("sum = {}", a.sum()); // calling .sum as a vec! of i32
    }
    fn traits_param(){

        use std::fmt::Debug; // default impl for debug trait comes from here

        #[derive(Debug)] // without it `Circle` cannot be formatted using `{:?}`
        struct Circle {
            radius: f64,
        }
        #[derive(Debug)] // without it `Square` cannot be formatted using `{:?}`
        struct Square {
            side: f64,
        }
        trait Shape {
            fn area(&self) -> f64;
        }
        impl Shape for Square {
            fn area(&self) -> f64 {
                self.side * self.side
            }
        }
        impl Shape for Circle {
            fn area(&self) -> f64 {
                self.radius * self.radius * std::f64::consts::PI
            }
        }
        fn print_info_1stw(shape: impl Shape + Debug){ // 1st way
            // Single argument passed shape: and impl for two traits Shape and Debug

            println!("{:?}", shape); // error[E0277]: `Circle` doesn't implement `Debug`
            println!("The area is {}", shape.area());
        }
        fn print_info_2ndw <T: Shape + Debug>(shape: T/*, shape2: T*/) {
            // more flexible for multiple arguments as shape2: upward

            println!("{:?}", shape); // error[E0277]: `Circle` doesn't implement `Debug`
            println!("The area is {}", shape.area());
        }
        fn print_info_3rdw <T>(shape: T/*, shape2: T*/)
        where T: Shape + Debug {
            // support one or more traits listed after where

            println!("{:?}", shape); // error[E0277]: `Circle` doesn't implement `Debug`
            println!("The area is {}", shape.area());
        }
        let c = Circle { radius: 2.0};
        print_info_1stw(c);
        // print_info_2ndw(c); // can't use all of them on same c, only one is active - below
        // print_info_3rdw(c); // error[E0382]: use of moved value: `c`
    }
    fn into(){
        use std::fmt::Debug; // default impl for debug trait comes from here

        #[derive(Debug)]
        struct Person
        {
            name: String
        }

        impl Person
        {
            fn new(name: &str) -> Person
            {
                Person { name: name.to_string() }
            }
            fn new_one<S: Into<String>>(name: S) -> Person
            {
                Person { name: name.into() }
            }
            fn new_second<S>(name: S) -> Person
            where S: Into<String>
            {
                Person { name: name.into() }
            }
        }

        let john = Person::new("John");
        let name = "Jane".to_string();
        let name_sec = "Jack".to_string();

        let jane_new = Person::new(name.as_ref());
        //.as_ref() converts String into &str
        // it's necessary to use in case upward bcs of arg: (name: &str)
        let jane_new_one = Person::new_one(name);
        // name can be reused, bcs in 1st case we pass ref
        // after this usage name will be dropped
        let jack_new_sec = Person::new_second(name_sec);

        println!(
            "direct assign Person::new() {:?}
            indirect with variables:
            without into(): {:?}
            with into(): {:?}
            with into() and where: {:?}",
            john,
            jane_new,
            jane_new_one,
            jack_new_sec
        );
    }
    fn drops(){
        struct Creature 
        {
            name: String            
        }
        impl Creature {
            fn new(name: &str) -> Creature {
                println!("{} enters the game", name);
                Creature { name: name.into() }
            }
        }
        impl Drop for Creature {
            fn drop(&mut self) {
                println!("{} is dead", self.name);
            }
        }

        let goblin = Creature::new("Jeff");
        println!("game proceeds");
        // goblin.drop(); //error[E0040]: explicit use of destructor method
        // drop(goblin); // manual usage, but in this part case isn't necessary

        let mut clever: Creature;
        // compiler tricks with reassign
        // Ben will survive the end and died right before Jeff
        {
            let smurf = Creature::new("Ben");
            println!("game proceeds");
            clever = smurf;
            println!("the end of scope")
        }
    }
    fn operator_overloading(){
        use std::ops::{Add, AddAssign};

        #[derive(Debug)] /*, PartialEq, Eq, Ord, PartialOrd*/
        struct Complex<T>
        {
            real: T,
            imaginary: T
        }

        impl<T> Complex<T>

        {
            fn new(real: T, imaginary: T) -> Complex<T>
            {
                Complex::<T> { real, imaginary }
            }
        }
        // impl Add for Complex<i32>
        // works only for i32
        // below one impl works for floating also
        // {
        //     type Output = Complex<i32>;
        //
        //     // a + b
        //     fn add(self, rhs: Self) -> Self::Output {
        //         Complex {
        //             real: self.real + rhs.real,
        //             imaginary: self.imaginary + rhs.imaginary
        //         }
        //     }
        // }
        impl<T> Add for Complex<T>
        where T: Add<Output = T>
        // still can't combine i32 + f32
        // error[E0308]: mismatched types
        {
            type Output = Complex<T>;

            // a + b
            fn add(self, rhs: Self) -> Self::Output {
                Complex {
                    real: self.real + rhs.real,
                    imaginary: self.imaginary + rhs.imaginary
                }
            }
        }
        impl<T> AddAssign for Complex<T>
            where T: AddAssign<T>
        {
            fn add_assign(&mut self, rhs: Self) {
                self.real += rhs.real;
                self.imaginary += rhs.imaginary;
            }
        }
        impl<T> Neg for Complex<T>
            where T: Neg<Output = T>
        {
            type Output = Complex<T>;

            fn neg(self) -> Self::Output {
                Complex {
                    real: -self.real,
                    imaginary: -self.imaginary
                }
            }
        }
        impl<T> PartialEq for Complex<T>
            where T: PartialEq
        {
            fn eq(&self, rhs: &Self) -> bool {
                self.real == rhs.real && self.imaginary == rhs.imaginary
            }
        }
        // impl<T: Eq> Eq for Complex<T>
        //     where T: Eq{}

        let mut a = Complex::new(1.0,2.0);
        let mut b = Complex::new(3.0,4.0);

        // comm out bcs error[E0382]: use of moved value: a
        // println!("Addition of Complex a + b:\n{:?}", a+b); // Add deb print

        // a += b; // cannot been used without impl
        // error[E0368]: binary assignment operation `+=` cannot be applied to type `Complex<{float}>
        // println!("Addition of Complex a += b:\n{:?}", a); // AddAssign deb print

        // println!("{:?}", -a); // Neg deb print

        // partial eq
        // full eq: x = x
        // NAN = not a number 0/0 inf/inf
        // NAN == NAN -> false


        println!("{}", a==b); // PartialEq deb print
        // error[E0369]: binary operation `==` cannot be applied to type `Complex<{float}>`
        // both == and != are impl by the same trait and can be used afterwords

    }
    trait Printable // dispatches shared trait
    {
        fn format(&self) -> String;
    }
    fn static_dispatch(){

        impl Printable for i32
        {
            fn format(&self) -> String {
                format!("i32: {}", *self)
            }
        }
        impl Printable for String
        {
            fn format(&self) -> String {
                format!("string: {}", *self)
            }
        }// monomorphisation

        fn print_it_mono(z: String)
        // you will get this repr at compile time for String
        {
            println!("{}", z.format());
        }

        fn print_it<T: Printable>(z: T)
        {
            println!("{}", z.format());
        }


        let a = 123;
        let b = "hello".to_string();

        // println!("{}\n{}", a.format(), b.format());

        print_it(a);
        print_it(b);



    }
    fn dynamic_dispatch(){
        // more expensive call with decision making in runtime

        fn print_it_too(z: &dyn Printable) // &dyn specifies for dyn dispatch
        {
            println!("{}", z.format());
        }

        let a = 321;
        let b = "olleh".to_string();

        // println!("{}\n{}", a.format(), b.format());

        print_it_too(&a);
        print_it_too(&b);


    }
    fn why_dyn_dispatch(){
        struct Circle { radius: f64 }
        struct Square { side: f64 }

        trait Shape {
            fn area(&self) -> f64;
        }
         impl Shape for Square {
             fn area(&self) -> f64 {
                 self.side * self.side
             }
         }
        impl Shape for Circle {
            fn area(&self) -> f64 {
                self.radius * self.radius * std::f64::consts::PI
            }
        }

        let shapes:[&dyn Shape; 4] = [
            &Circle { radius: 1.0 },
            &Square { side: 3.0 },
            &Circle { radius: 2.0 },
            &Square { side: 4.0 },
        ];
        for (i, shape) in shapes.iter().enumerate()
        {
            println!("Shape #{} has area {}", i, shape.area());
        }
    }
    fn vec_of_diff_obj(){
        enum Creature
        {
            Human(Human),
            Cat(Cat)
        }
        let mut creatures = Vec::new();
        // error[E0308]: mismatched types
        // creatures.push(Human {name: "John"});
        // creatures.push(Cat {name: "Fluffy"});

        creatures.push((Creature::Human(
            Human { name: "John" }
        )));
        creatures.push(Creature::Cat(
            Cat { name: "Fluffy" }
        ));
        for c in creatures
        {
            match c {
                Creature::Human(h) => h.talk(),
                Creature::Cat(c) => c.talk()
            }
        }

        let mut animals: Vec<Box<dyn Animal>> = Vec::new();
        // error[E0277]: the size for values of type `dyn Animal` cannot be known at compilation time
        // doesn't have a size known at compile-time
        animals.push(
            Box::new(Human {name: "John"}));
        animals.push(
            Box::new(Cat {name: "Fluffy"}));

        for a in animals.iter() { a.talk(); }
    }


    // traits_intr();
    //traits_param();
    //into();
    //drops();
    //operator_overloading();
    //static_dispatch();
    //dynamic_dispatch();
    // why_dyn_dispatch()
    //vec_of_diff_obj();


}

fn lifetime_and_memory(){
    fn ownership(){
        let v = vec![1,2,3];

        //let v2 = v; // pointer MOVEd
        //println!("{:?}", v); // error[E0382]: borrow of moved value: `v`

        // let foo = |v:Vec<i32>| ();
        // foo(v); // pointer MOVEd
        // println!("{:?}", v); //error[E0382]: borrow of moved value: `v`

        // let u= 1; // i32
        let u= Box::new(1); //
        let u2 = u; // error[E0382]: borrow of moved value: `u`
        // println!("u = {}", u); // not MOVEd but copied

        let print_vector = |x:Vec<i32>| -> Vec<i32>
            {
                println!("{:?}", x);
                x
            };
        let vv = print_vector(v);
        println!("{}", vv[0]);
    }
    fn borrowing(){
        let print_vector = |x:&Vec<i32>|
            {
                println!("x[0] = {}", x[0]);
            };

        let v = vec![3,2,1];
        print_vector(&v);

        //x.push(1,2,3);

        println!("v[0] = {}", v[0]);

        let mut a = 40;
        let c = &a;
        {
            // strange thing, if you not use b after immutable borrow of a,
            // there will not be any complains from compiler -> scope { } isn't enforced
            let b = &mut a;
            *b +=2;
        }
        // println!("{}", c); // error[E0502]: cannot borrow `a` as mutable because it is also borrowed as immutable
        println!("a = {}", a);

        // *b -=2; // mutable borrow later vs. immutable borrow
        // error[E0502]: cannot borrow `a` as immutable because it is also borrowed as mutable

        let mut z = vec![3,4,5];

        for i in &z
        {
            println!("i = {}", i);
            // z.push(5); // cant do it mutable borrow vs. immutable borrow
        }
    }
    fn lifetimes(){
        // &'static str // live as long as program

        struct Person
        {
            name: String
        }

        impl Person
        {
            fn get_ref_name(&self) -> &String
            // fn get_ref_name<'a>(&'a self) -> &'a String
            // Explicit lifetimes given in parameter types where they could be elided
            {
                &self.name
            }
        }
        struct Company<'z>
        {
            name: String,
            ceo: &'z Person
        }
        let boss = Person { name: String::from("Elon Musk") };
        let tesla = Company { name: String::from("Tesla"), ceo: &boss };

        let mut z: &String;

        let p = Person { name: String::from("John") };
        z = p.get_ref_name();



    }
    fn lifetime_struct_impl(){
        struct Person<'a>
        {
            name: &'a str
        }
        impl<'b> Person<'b> // error[E0726]: implicit elided lifetime not allowed here
        //   ^^^^^^- help: indicate the anonymous lifetime: `<'_>`
        {
            fn talk(&self){
                println!("Hi, my name is {}", self.name);
            }
        }

        let person = Person { name: "Lexk" }; // error[E0106]: missing lifetime specifier
        //                          ^ expected named lifetime parameter

        person.talk();





    }
    fn reference_counted_var(){
        use std::rc::Rc; // allow var which keeps # of references

        struct Person
        {
            name: Rc<String>
        }
        impl Person
        {
            fn new(name: Rc<String>) -> Person
            {
                Person { name } // { name: name } - can be simplified to { name }
            }
            fn great(&self)
            {
                println!("Hi, my name is {}", self.name);
            }
        }
        fn rc_demo(){
            let name = Rc::new("John".to_string());
            println!("name = {}, has {} strong pointers before", name, Rc::strong_count(&name));
            {
                let person = Person::new(name.clone()); // .clone value borrowed
                //                               ---- value moved here
                // error[E0382]: borrow of moved value: `name`
                // Rc::strong_count will be increased +1 with .clone & afterwords name reusable
                println!("name = {}, has {} strong pointers after .clone ", name, Rc::strong_count(&name));
                println!("Name = {}", name);
                person.great();
            }// error[E0382]: borrow of moved value: `name`
            println!("name = {}, has {} strong pointers after .great ", name, Rc::strong_count(&name));
        }
        rc_demo();
    }
    fn atomic_reference_counted_vars(){
        use std::thread;
        use std::sync::Arc; // Atomic reference counter

        struct Person
        {
            name: Arc<String>
        }
        impl Person
        {
            fn new(name: Arc<String>) -> Person
            {
                Person { name } // { name: name } - can be simplified to { name }
            }
            fn great(&self)
            {
                println!("Hi, my name is {}", self.name);
            }
        }
        fn arc_demo(){
            let name = Arc::new("John".to_string());
            let person = Person::new(name.clone());

            // new error msg body introduced --> check it online
            let t = thread::spawn(move || {
                //                ^^^^^^^^^^^^^_- Rc<String>` cannot be sent between threads safely
                // error[E0277]: `Rc<String>` cannot be sent between threads safely
                person.great();
            });
            println!("name = {}", name);

            t.join().unwrap();
            // TODO: checks specs and docs on 'thread::spawn(move ||', '.join().unwrap();'
        }
        arc_demo();
    }
    fn mutex_thread_safe_mutability(){
        use std::thread;
        use std::sync::{Arc, Mutex}; // Atomic reference counter

        struct Person
        {
            name: Arc<String>,
            state: Arc<Mutex<String>>
        }
        impl Person
        {
            fn new(name: Arc<String>, state: Arc<Mutex<String>>) -> Person
            {
                Person { name, state } // { name: name } - can be simplified to { name }
            }
            fn great(&self)
            {
                let mut state = self.state.lock().unwrap();
                state.clear();
                state.push_str("excited");
                println!("Hi, my name is {} and I'm {}", self.name, state.as_str());
            }

        }
        fn mutex_demo(){
            let name = Arc::new("John".to_string());
            let state = Arc::new(Mutex::new("bored".to_string()));
            let person = Person::new(name.clone(), state.clone());
            let t = thread::spawn(move || {
                person.great();
            });

            println!("name = {}, state = {}", name, state.lock().unwrap().as_str());
            t.join().unwrap();
        }
        mutex_demo(); // TODO: check docs for Mutex
    }

    //ownership();
    //borrowing();
    //lifetimes();
    //lifetime_struct_impl()
    //reference_counted_var();
    //atomic_reference_counted_vars();
    // mutex_thread_safe_mutability();
}

fn advance_top(){

    // student* --- *course

    fn cir_ref_refcell(){ // No RefCell try impl below

        // commented below one as example of mind map

        struct Student<'a>
        {
            name: String,
            courses: Vec<&'a Course<'a>>
        }
        impl<'a> Student<'a>
        {
            fn new(name: &str) -> Student<'a>
            {
                Student {
                    name: name.into(),
                    courses: Vec::new()
                }
            }
        }
        struct Course<'a>
        {
            name: String,
            students: Vec<&'a Student<'a>>
        }
        impl<'a> Course<'a>
        {
            fn new(name: &str) -> Course<'a>
            {
                Course {
                    name: name.into(),
                    students: Vec::new()
                }
            }
            fn add_student(&'a mut self,
            student: &'a mut Student<'a>)
            {
                student.courses.push(self);
                // self.students.push(student);
                // error[E0502]: cannot borrow `self.students` as mutable because it is also borrowed as immutable
            }
        }
    }
    fn cir_ref_rc_refcell() { // Rc + RefCell impl below
        // commented below one as example of mind map

        use std::rc::Rc;
        use std::cell::RefCell;

        struct Student
        {
            name: String,
            courses: Vec<Rc<RefCell<Course>>>
        }
        impl Student
        {
            fn new(name: &str) -> Student
            {
                Student {
                    name: name.into(),
                    courses: Vec::new()
                }
            }
        }

        struct Course
        {
            name: String,
            students: Vec<Rc<RefCell<Student>>>
        }
        impl Course
        {
            fn new(name: &str) -> Course
            {
                Course {
                    name: name.into(),
                    students: Vec::new()
                }
            }
            fn add_student(
                course: Rc<RefCell<Course>>,
                student: Rc<RefCell<Student>>
            ) {
                student.borrow_mut().courses.push(course.clone());
                course.borrow_mut().students.push(student.clone());
            }
        }

        let john = Rc::new(
            RefCell::new(
                Student::new("John")
            )
        );
        let jane = Rc::new(
            RefCell::new(
                Student::new("Jane")
            )
        );

        let course = Course::new("Rust Course");

        let magic_course = Rc::new(RefCell::new(course));

        // course.add_student(john); // Rc

        Course::add_student(magic_course.clone(), john);
        Course::add_student(magic_course, jane);
    }
    fn avoid_cir_ref_redis(){ // avoiding circle reference redesign below
        // students
        // courses
        // Vec < Enrollment { course, student } >
        use std::rc::Rc;
        use std::cell::RefCell;

        struct Student {
            name: String
        }
        impl Student {
            fn courses (&self, platform: /*&*/Platform) -> Vec<String>
            {
                platform.enrollments.iter()
                    .filter(|&e| e.student.name == self.name)
                    .map(|e| e.course.name.clone())
                    .collect()
            }
        }
        struct Course {
            name: String
        }
        struct Enrollment<'a> {
            student: &'a Student,
            course: &'a Course
        }
        impl<'a> Enrollment <'a> {
            fn new(student: &'a Student, course: &'a Course) -> Enrollment<'a>
            {
                Enrollment { student, course }
            }
        }
        struct Platform <'a> {
            enrollments: Vec<Enrollment<'a>>
        }
        impl<'a> Platform<'a> {
            fn new() -> Platform<'a> {
                Platform {
                    enrollments: Vec::new()
                }
            }
            fn enroll(
                &mut self,
                student: &'a Student,
                course: &'a Course)
            {
                self.enrollments.push(
                    Enrollment::new(student, course)
                )
            }
        }
        let john = Student {
            name: "John".into()
        };
        let course = Course {
            name: "Intro to Rust".into()
        };
        let mut platform = Platform::new();
        platform.enroll(&john, &course);
        for c in john.courses(platform) {
            println!("John is taking {} course", c);
        }
    }

    //cir_ref_refcell();
    //cir_ref_rc_refcell();
    //avoid_cir_ref_redis();
}

fn concurrency(){
    fn handle_threads(){
        use std::thread;
        use std::time;

        let handle= thread::spawn(||{
            for _ in 1..10 {
                print!("+");
                thread::sleep(time::Duration::from_millis(500));
            }
        });
        for _ in 1..10 {
            print!("-");
            thread::sleep(time::Duration::from_millis(300));
        }

        handle.join();
        // with .join() "-+-+--+-+--+--++++"
        // without .join() "-+-+--+-+--+--+"
        // second one exits before handle finishes it's work!
    }
    // handle_threads();
}

fn odds_ends(){
    fn consume_crate(){
        extern crate rand;
        use rand::Rng;

        let mut rng = rand::thread_rng();
        let b:bool = rng.gen();
    }
    fn bld_mod_crates(){
        // in Cargo.toml:
        // phrases = { path = "./Phrases" }
        extern crate phrases; // ./Phrases/src/lib.rs
        use phrases::greetings::french;

        println!("English: {}, {}",
                 phrases::greetings::english::hello(), // ./Phrases/src/greetings/english.rs
                 phrases::greetings::english::goodbye()
        );
        println!("French: {}, {}",
        french::hello(), // use phrases::greetings::french;
        french::goodbye());
    }
    fn tests(){
        // cd Phrases/
        // cargo test

        println!("Tests for Phrases mod are located:
        ./Phrases/tests/lib.rs");
    }
    //consume_crate();
    // bld_mod_crates();
    //tests();
}

fn main() {

    types_and_variables(); // part 2: Types and Variables
    control_flow(); // part 3: Control Flow
    data_structures(); // part 4: Data Structures
    standard_collections(); // part 5: Standard Collections
    characters_strings(); // part 6: Characters and Strings
    functions_and_arguments(); // part 7: Functions and function arguments
    traits(); // part 8: Traits
    lifetime_and_memory() ; //part 9: Lifetime and Memory
    advance_top(); // part 10: Advance Topics
    concurrency(); // part 11: Concurrency
    odds_ends(); // part 12: Odds & Ends




    //TODO:
    // Review fill with examples, uncomment & correct p.8.5: Operator Overloading
    // ownership uncomment and make it work 9.1
    // add docs + possible refactor


}