fn how_many(x:i32) -> &'static str
{
    match x
    {
        0 => "no",
        1 | 2 => "one or two",
        12 => "a dozen",
        z @ 9..=11 => "lots of",  // z @ casted for particular range
        // don't use ... for inclusive range, but ..= instead
        _ if (x % 2 == 0) => "some",
        _ => "a few"
    }
}

pub fn pattern_matching(){
    for x in 0..13
    {
        println!("{}: I have {} oranges", x, how_many(x));
    }

    let mut point = (4,7);

    match point
    {
        (0,0) => println!("origin"),
        (0,y) => println!("x axis, y = {}", y),
        (ref mut x, 0) => println!("y is axis, x = {}", x),
        //above x is passed as reference and mutable, so you can mod if
        (_,y,) => println!("(?,{})",y)
    }
    //below part is copied from structures part and modified
    enum Color
    {
        Red,
        Green,
        Blue,
        RgbColor(u8, u8, u8), //tuple
    CMYKColor{cyan:u8, magenta: u8, yellow: u8, black: u8} // struct
    }
    let c:Color = Color::CMYKColor {cyan: 0, magenta: 0, yellow: 0, black: 255};
    //let c:Color = Color::RgbColor(0,0,0);
    //let c:Color = Color::Red;
    match c
    {
        Color::Red => println!("r"),
        Color::Green => println!("g"),
        Color::Blue => println!("b"),
        Color::RgbColor(0, 0, 0)
        | Color::CMYKColor{black: 255, ..} => println!("black"),
        //modified upward with , .. to taking in consideration only black option
        Color::RgbColor(r,g,b) => println!("rgb({}, {}, {})", r,g,b),
        _ => println!("Not Listed Color")
    }
}